# docker-wp-gitlab

Docker setup to host wordpress server

### Clone the repository

`git clone https://gitlab.com/ghuth/docker-wp-gitlab`

### Start up the container
Open the project directory 

`cd docker-wp-gitlab`

and run the following command:

`docker-compose up -d`

You can see the status of the container with this command:

`docker ps`

It typically takes a minute or two to spin up everything depending on your machine and your internet connection.

http://localhost:8000

Login panel:

http://localhost:8000/wp-admin/
